export function getClosestNodes (node, nodesMap) {
    const rs = []
    if (!node || !nodesMap) {
        return rs
    }
    const loop = (node) => {
        if (!node) {
            return
        }
        rs.push(node)
        loop(node.parent)
    }
    loop(node)
    return rs
}

export function newNode (data, idKey) {
    data = data || {}
    if (data[idKey] === null || data[idKey] === undefined || data[idKey] === '') {
        throw new Error('节点唯一标识必须')
    }
    const node = {
        id: data[idKey],
        level: 1,
        height: 0,
        expanded: false,
        removed: false,
        parent: null,
        data
    }
    return node
}