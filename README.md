# TreeSelect 多级树选择

TreeSelect多级树选择主要用来解决海量数据下的渲染性能问题，目前已经支持以下特性：

- [x] 默认展开指定层级
- [x] 默认展开指定节点
- [x] 动态插入节点
- [x] 动态移除节点
- [x] 覆盖节点数据
- [x] 本地过滤
- [x] loading效果
- [x] 选中事件   
- [x] 按需滚动渲染DOM

## 安装
```
npm install @mijs/vue-tree-select
```
## 使用

### 属性配置
|  名称   | 说明  | 类型  | 可选值  | 默认值  |
|  ----  | ----  | ----  | ----  | ----  |
| data  | 数据源，注意data需要处理为树形结构再传递给组件使用 | Array | -- | [] |
| loading  | 是否显示loading遮罩层 | Boolean | -- | false |
| height  | 当前组件高度 | Boolean | -- | false |
| itemHeight  | 节点的高度 | Number | -- | 24 |
| idKey  | 节点唯一key | String | -- | id |
| labelKey  | 节点显示文本key | String | -- | label |
| childrenKey  | 子级节点key | String | -- | children |
| defaultExpandLevel  | 默认展开层级 | Number | -- | -- |
| defaultExpandKeys  | 默认展开的节点key集合 | Array | -- | -- |
| filterMethod  | 过滤方法，接受{node: Object, keyword: String}参数 | Function | -- | -- |
| current  | 当前选中的key | String | -- | -- |

### 方法
|  名称   | 说明  | 参数  | 返回值  |
|  ----  | ----  | ----  | ----  |
| insertNode  | 向指定节点末尾插入节点 | data: 插入的节点数据，parentNode: 父及节点 | -- |
| removeNode  | 移除节点 | nodeId：节点或节点id | -- |
| filter  | 过滤数据 | keyword：过滤的关键词 | -- |

### 事件
|  名称   | 说明  | 参数   |
|  ----  | ----  | ----  |
| current  | 当前行改变触发时间 | { node: Object } |

### 插槽
|  名称   | 说明  | 作用域  | 
|  ----  | ----  | ----  |
| default  | 节点内容区域插槽，可覆盖节点文本 | { node: Object } |
| addon  | 节点右侧工具扩展区域插槽 | { node: Object } |

### node节点对象
|  名称   | 说明  |
|  ----  | ----  |
| id  |  | 节点唯一标识 |
| level  | 节点层级 |
| expanded  | 是否展开 |
| parent  | 父级节点 |
| data  | 节点的元素数据 |

### 注意事项
- 本组件主要解决海量数据下的性能问题，所以使用时必须指定固定的高度
- 目前组件提供的功能并不是很多，可以通过fock仓库进行扩展